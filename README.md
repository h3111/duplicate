# _Find a duplicate in a list_ in Haskell


This program is inspired by
[_What are the best programming interview questions you've ever asked or been asked?_](https://qr.ae/pGWDFt).


## The challenge

Take a non-sorted list of _Int_ values in the range _1 .. n_. The length of this
list is _n + 1_. Therefore, the list must contain at least one duplicate (maybe
more).

Example: For _n = 4_, the list could be _[4, 1, 3, 3, 2]_, but _[4, 1, 3, 3, 1]_
is also possible.

Return a duplicate, the list contains. If there is more than one duplicate, it
is sufficient to output one of them. So, for the first list of the example above
return _3_, and for the second list return _1_ or _3_.


## Some thoughts of mine

What comes to my mind, when I read these requirements:

* Can I be sure that the list always contains at least one duplicate? Can the
  list even be empty?

* Will the smallest list value always be _1_?

* Can the list be _really_ large?

* May I count on the fact that the list contains just _Int_ values as described
  above? Or is it possible that the customer comes along some day and tells me
  that now I have to deal with a list, containing elements other than _Int_
  (e.g. _Char_)?

* Is it really sufficient to get just one of the duplicates, if there is more
  than one duplicate in the list? What if the customer changes his mind and
  wants _all_ duplicates now?

My customer experience tells me that I have at least to take into account
"sudden" requirement changes like these. I'm distrustful.


## Implementation


### The easy way

Okay, to get used to the requirements, let's play around with them and implement
the following function:

````haskell
findDuplicate :: Eq a => [a] -> Maybe a
findDuplicate [] = Nothing
findDuplicate (x:xs) =
  if x `elem` xs
    then Just x
    else findDuplicate xs
````

The function takes a list of _any_ type that is a member of the type class _Eq_.
The function is able to handle empty lists as well as lists with no duplicates.

````haskell
>>> findDuplicate [4, 1, 3, 3, 2]
Just 3

>>> findDuplicate [4, 1, 3, 3, 1]
Just 1

>>> findDuplicate "ABCDEAB"
Just 'A'

>>> findDuplicate ["Hello", "Hello", "World", "World"]
Just "Hello"

>>> findDuplicate []
Nothing
````

A slight modification returns a list of _all_ duplicates in the given list:

````haskell
findDuplicates :: Eq a => [a] -> [a]
findDuplicates [] = []
findDuplicates (x:xs) =
  if x `elem` xs
    then x : findDuplicates xs
    else findDuplicates xs
````

Let's call the function with the same values as above:

````haskell
>>> findDuplicates [4, 1, 3, 3, 2]
[3]

>>> findDuplicates [4, 1, 3, 3, 1]
[1,3]

>>> findDuplicates "ABCDEAB"
"AB"

>>> findDuplicates ["Hello", "Hello", "World", "World"]
["Hello","World"]

>>> findDuplicates []
[]
````

So far, so good: The functions fulfill all (maybe modified) requirements,
mentioned above--but except of one: If the given list is _really_ large (50,000
elements or more) and the duplicate is "far away" from the start of the list,
then the solution sucks; it just takes too much time (at least with my old
faithful Linux notebook).


### Speed it up

Let's take into account that we have a list of _n + 1_ values, where the values
are _Int_ values. _Int_ is a member of the type classes _Enum_ and _Ord_. (The
latter does of course not necessarily mean that the list has to be ordered.) The
list element values are all in the range _1 .. n_. (In fact, the list range may
be from _m_ to _n_, where _m_ and _n_ may even be negative. It is just the
number of list elements that counts.)

Let us define the range as follows: _range_ = _lower_ .. _upper_. The number of
possible list elements in _range_, if there is no duplicate, is
_upper - lower + 1_.

In other words: If our list consists of _upper - lower + 1_ elements, then is
does not contain any duplicate (which is good to know).

Now, we can split the range into two sub-ranges: The first sub-range contains
all elements from _1_ to _n/2_ (rounded down to the next _Int_ value, if _n_ is
odd), and the second sub-range contains all elements from _n/2+1_ to _n_.

For both sub-ranges, we compare the number of list elements with the possible
number of elements in the related sub-range, and discard the sub-range, where
both values are equal (since the duplicate is not located inside this
sub-range).

Now, we again split the remaining sub-range into two sub-ranges recursively, as
described above, until _lower_ and _upper_ of the related range are equal. This
value is the duplicate (or at least one of them).


### Make it more abstract

As mentioned above, we have to split ranges into "half" sub-ranges. Therefore,
the range value types must be members of the type class _Enum_. _Int_ is such
a type, and so is for example _Char_ and even _Bool_. (But _String_ is not.)

So, there is no need to limit the implementation just to _Int_ values. In
[duplicate.hs](duplicate.hs), the constraint `Enum a, Ord a` is used. Watch that
`Eq` is a superclass of `Ord`. So, the constraint is in fact
`Enum a, Eq a, Ord a`. (_Ord_ is needed for the functions _min_, _minimum_, and
_maximum_.)

In [duplicate.hs](duplicate.hs), lists of the types _Int_, _String_, _Bool_, and
the _WeekDay_ are used.

Although _Integer_ also fulfills the `Enum a, Eq a, Ord a` constraint, the
solution in [duplicate.hs](duplicate.hs) does not work for _Integer_, because
the functions _toEnum_ and _fromEnum_ use _Int_ values (instead of _Integer_
values). The following test would fail, due to an _Int_ overflow:

````haskell
{-|
  Returns a list of Integer with one duplicate.

  >>> createIntegers
  [9223372036854775808,9223372036854775808,9223372036854775809,9223372036854775810]
-}
createIntegers :: [Integer]
createIntegers = [value, value, value + 1, value + 2]
  where
    value = toInteger (maxBound :: Int) + 1
````


## Using  [duplicate.hs](duplicate.hs)


### Compiling

````bash
$ sudo apt install haskell-platform hlint
$ ghc --make -Wall duplicate.hs
````

### Linting

````bash
$ hlint -s duplicate.hs
````

### "Installation"

Copy `duplicate` to a directory that is listed in the environment variable
`PATH`, e.g. `~/bin` (or create a symlink).


### Usage

Call `duplicate` this way to get a short usage message:

````bash
$ duplicate -h
Usage: duplicate [-t|-T] [LIST]
````

Find the duplicate in the _Int_ list _[4, 1, 3, 3, 2]_:

````bash
$ duplicate 4 1 3 3 2

list:      [4,1,3,3,2]
duplicate: 3
````

Find one of the duplicates in the _Int_ list _[4, 1, 3, 3, 1]_:

````bash
$ duplicate 4 1 3 3 1

list:      [4,1,3,3,1]
duplicate: 3
````

Don't let yourself be fooled by an empty list:

````bash
$ duplicate

list:      ""
duplicate: none

WARNING:   list length not greater than range size!
````

...or a list that does not contain a duplicate:

````bash
$ duplicate 1 2 3 4

list:      [1,2,3,4]
duplicate: none

WARNING:   list length not greater than range size!
````

...or a list with an insufficient size:

````bash
$ duplicate 1 1 3 4

list:      [1,1,3,4]
duplicate: none

WARNING:   list length not greater than range size!
````

Since _9223372036854775808_ does not fit into an _Int_, handle the arguments as
_String_ list, take the first (_"9223372036854775806"_), and find one of the
duplicate characters in it (_'2'_):

````bash
$ duplicate 9223372036854775806 9223372036854775806 9223372036854775807 9223372036854775808

list:      "9223372036854775806"
duplicate: '2'
````

Find one duplicate in a _String_:

````bash
$ duplicate hhheeelllooo

list:      "hhheeelllooo"
duplicate: 'o'
````

As expected, this does not work

````bash
$ duplicate hello

list:      "hello"
duplicate: none

WARNING:   list length not greater than range size!
````

Find the duplicate _18_ in a list of _Int_ from _1_ to _19_:

````bash
$ duplicate -t 1 19 18

list:      [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,18,19]
duplicate: 18
````

Find the duplicate _42_ in a list of _Int_ from _1_ to _1000000_. The number
in parentheses _(1000001)_ is the list length.

````bash
$ duplicate -t 1 1000000 42

list:      [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20] ... (1000001)
duplicate: 42
````

Find the duplicate in a list of _Bool_, in a list of _WeekDay_, and a _String_
with all 1114113 available Unicode characters:

````bash
$ duplicate -T

list:      [True,False,True]
duplicate: True

list:      [Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday]
duplicate: Sunday

list:      "W\NUL\SOH\STX\ETX\EOT\ENQ\ACK\a\b\t\n\v\f\r\SO\SI\DLE\DC1\DC2" ... (1114113)
duplicate: 'W'
````

<sub>[Wolfgang](mailto:haskell@wu6ch.de)</sub>


<!-- EOF -->
