{-|
  duplicate - find a duplicate in a list

  Usage:    duplicate [-t|-T] [LIST]

  Examples: duplicate                 # empty list
            duplicate 3 1 1 3         # list of Int
            duplicate '"3113"'        # list of Char (quoted, due to digits)
            duplicate "'3113'"        # list of Char (quoted, due to digits)
            duplicate "caac"          # list of Char
            duplicate -t 1 1000 500   # test list of Int
            duplicate -T              # test lists of Bool, WeekDay, and Char

  Compile:  ghc --make -Wall duplicate.hs
  Lint:     hlint -s duplicate.hs

  Author:   Wolfgang <haskell@wu6ch.de>

  Inspired by:

  Leo Polovets' answer to the question:
  "What are the best programming interview questions
  you've ever asked or been asked?"
  (https://qr.ae/pGWDFt)
-}


{-|
  Needed for 'forall a.'.

  https://stackoverflow.com/questions/38911523/mismatch-of-rigid-type-variables
  https://mail.haskell.org/pipermail/beginners/2012-November/010980.html
-}
{-# LANGUAGE ScopedTypeVariables #-}


import Control.Applicative ((<|>))
import Control.Monad ((>=>))

import Data.Bool (bool)
import Data.List (foldl')
import Data.Maybe (fromMaybe, maybe)

import System.Environment (getArgs)

import Text.Read (readMaybe)


{-|
  Range is a pair, where the first pair element defines the lower limit, and the
  second pair element defines the upper limit of the range.
-}
newtype Range a =
  Range
    { unRange :: (a, a)
    }
  deriving (Show)


{-|
  WeekDay is a sum type that is a member of the needed type classes. Therefore,
  a list of WeekDay can be used as test case (see function 'createWeekDays').
-}
data WeekDay
  = Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday
  | Saturday
  | Sunday
  deriving (Bounded, Enum, Eq, Ord, Show)


{-|
  Takes a Range and returns True, if lower and upper limit are equal.

  >>> rangeLimitsEqual $ Range (2,5)
  False

  >>> rangeLimitsEqual $ Range (2,2)
  True

  >>> rangeLimitsEqual $ Range ('A','B')
  False

  >>> rangeLimitsEqual $ Range ('A','A')
  True

  >>> rangeLimitsEqual $ Range (Monday,Monday)
  True
-}
rangeLimitsEqual :: Eq a => Range a -> Bool
rangeLimitsEqual = uncurry (==) . unRange


{-|
  Takes a Range and returns its size, that is, the number of size elements
  (negative, if the upper limit of the range is less than the lower limit).

  >>> getRangeSize $ Range (1,1)
  1

  >>> getRangeSize $ Range (3,7)
  5

  >>> getRangeSize $ Range (7,3)
  -5

  >>> getRangeSize $ Range ('A','Z')
  26

  >>> getRangeSize $ Range (Monday,Sunday)
  7

  >>> getRangeSize $ Range (Sunday,Monday)
  -7
-}
getRangeSize :: (Enum a) => Range a -> Int
getRangeSize (Range (lowerLim, upperLim)) = d + bool 1 (-1) (d < 0)
  where
    d = fromEnum upperLim - fromEnum lowerLim


{-|
  Takes a list and returns a Range, depending on the list minimum and maximum.

  >>> getListRange "ABC"
  Range {unRange = ('A','C')}

  >>> getListRange [5,6,1,7,3]
  Range {unRange = (1,7)}

  >>> getListRange [Wednesday,Friday,Monday]
  Range {unRange = (Monday,Friday)}

  >>> getListRange []
  Range {unRange = ((),())}
-}
getListRange :: (Enum a, Ord a) => [a] -> Range a
getListRange [] = let dummy = toEnum 0 in Range (dummy, dummy)
getListRange l  = curry Range <$> minimum <*> maximum $ l


{-|
  Takes a Range and a list and returns True, if the list length is greater than
  the range size.

  >>> checkListRange (Range (1,4)) [1,1,3,4,5]
  True

  >>> checkListRange (Range (1,4)) [1,1,3,4]
  False
-}
checkListRange :: (Enum a) => Range a -> [a] -> Bool
checkListRange r l = length l > getRangeSize r


{-|
  Takes a Range, splits it into two ranges, and returns these two ranges as
  a pair.

  >>> splitRange $ Range (1,10)
  (Range {unRange = (1,5)},Range {unRange = (6,10)})

  >>> splitRange $ Range (3,7)
  (Range {unRange = (3,4)},Range {unRange = (5,7)})

  >>> splitRange $ Range ('A','Z')
  (Range {unRange = ('A','M')},Range {unRange = ('N','Z')})

  >>> splitRange $ Range (Monday,Sunday)
  (Range {unRange = (Monday,Thursday)},Range {unRange = (Friday,Sunday)})
-}
splitRange :: (Enum a, Ord a) => Range a -> (Range a, Range a)
splitRange r = (fstRange, sndRange)
  where
    (lowerLim, upperLim) = unRange r
    iLower               = fromEnum lowerLim
    iUpper               = fromEnum upperLim
    iMid                 = min (iLower `div` 2 + iUpper `div` 2) iUpper
    fstRange             = Range (min lowerLim upperLim, toEnum iMid)
    sndRange             = Range (toEnum (min (iMid + 1) iUpper), upperLim)


{-|
  Takes a Range and a list, counts the list elements that are in Range 'r',
  ('count'), and returns True, if 'count' is greater than the size of 'r'.

  >>> check (Range ('A', 'C')) "ABCD"
  False

  >>> check (Range ('A', 'C')) "ABCDD"
  False

  >>> check (Range ('A', 'C')) "ABCCD"
  True

  >>> check (Range ('A', 'C')) "ABBCD"
  True

  >>> check (Range ('A', 'C')) "AABCD"
  True
-}
check :: forall a. (Enum a, Ord a) => Range a -> [a] -> Bool
check r l = count /= 0 && count > getRangeSize r
  where
    (lowerLim, upperLim) = unRange r
    count                = foldl' foldFunc 0 l
    foldFunc :: (Enum a, Ord a) => Int -> a -> Int
    foldFunc acc element
      | lowerLim <= element && element <= upperLim = acc + 1
      | otherwise                                  = acc


{-|
  Takes a Range 'r' and a list, tries to find at least one duplicate in 'r',
  and returns this duplicate, if any, otherwise Nothing.
-}
findDuplicate' :: (Enum a, Ord a) => Range a -> [a] -> Maybe a
findDuplicate' r l
  | not rangeCheck                   = Nothing
  | rangeLimitsEqual r && rangeCheck = Just $ fst $ unRange r
  | otherwise                        = findDuplicate r l
  where
    rangeCheck = check r l


{-|
  Takes a Range 'r' and a list, splits 'r', tries to find at least one duplicate
  in one of the sub-ranges, and returns this duplicate, if any, otherwise Nothing.

  >>> findDuplicate (Range ('A', 'D')) "ABCD"
  Nothing

  >>> findDuplicate (Range ('A', 'D')) "AABCD"
  Just 'A'

  >>> findDuplicate (Range ('A', 'D')) "ABCDD"
  Just 'D'

  >>> findDuplicate (Range ('A', 'D')) "ABCCCDD"
  Just 'C'
-}
findDuplicate :: (Enum a, Ord a) => Range a -> [a] -> Maybe a
findDuplicate r l = findDuplicate' fstRange l <|> findDuplicate' sndRange l
  where
    (fstRange, sndRange) = splitRange r


{-|
  Takes a list, outputs the first 20 elements on 'stdout', evaluates duplicates,
  and outputs the first duplicate on 'stdout' (if any).
-}
printDuplicate' :: (Enum a, Ord a, Show a) => [a] -> IO ()
printDuplicate' l =
  putStrLn $
  mconcat
    [ "\nlist:      ", show $ take maxPrintedLength l, suffix
    , "\nduplicate: ", maybe "none" show $ findDuplicate r l
    , bool warn "" $ checkListRange r l
    ]
  where
    r                = getListRange l
    maxPrintedLength = 20
    actualLength     = length l
    p                = actualLength <= maxPrintedLength
    suffix           = bool (" ... (" <> show actualLength <> ")") "" p
    warn             = "\n\nWARNING:   list length not greater than range size!"


{-|
  Takes an Integer value and returns this value as Maybe Int. If the Integer
  value is too small or too great for an Int, the function returns Nothing.

  >>> integerToMaybeInt (9223372036854775807)
  Just 9223372036854775807

  >>> integerToMaybeInt (9223372036854775808)
  Nothing

  >>> integerToMaybeInt (-9223372036854775808)
  Just (-9223372036854775808)

  >>> integerToMaybeInt (-9223372036854775809)
  Nothing
-}
integerToMaybeInt :: Integer -> Maybe Int
integerToMaybeInt i
  | i < toInteger (minBound :: Int) = Nothing
  | i > toInteger (maxBound :: Int) = Nothing
  | otherwise                       = Just $ fromInteger i


{-|
  Takes a list of String (the command line arguments) and returns a list of Int,
  if all given command line arguments are numbers, otherwise an empty list.

  >>> argsToInts []
  []

  >>> argsToInts []
  [""]

  >>> argsToInts ["Hello","42"]
  []

  >>> argsToInts ["42","43"]
  [42,43]

  >>> argsToInts ["42", "9223372036854775808"]
  []
-}
argsToInts :: [String] -> [Int]
argsToInts = fromMaybe [] . mapM (readMaybe >=> integerToMaybeInt)


{-|
  Takes a list of String (the command line arguments) and returns the first list
  element with '"' and '\'' removed.

  >>> argsToString []
  ""

  >>> argsToString [""]
  ""

  >>> argsToString ["'Hello'","42"]
  "Hello"
-}
argsToString :: [String] -> String
argsToString []   = []
argsToString args = filter (`notElem` "\"'") $ head args


{-|
  Takes a list of String (the command line arguments) and calls the polymorphic
  function printDuplicate' with either a list of Int or a String.
-}
printDuplicate :: [String] -> IO ()
printDuplicate args = do
  let ints  = argsToInts args
  let chars = argsToString args
  if  null ints then printDuplicate' chars else printDuplicate' ints


{-|
  Takes a list of Int and returns a related list of Int.

  >>> createInts []
  []

  >>> createInts [10]
  [1,2,3,4,5,6,7,8,9,10]

  >>> createInts [5,10]
  [5,6,7,8,9,10]

  >>> createInts [1,10,3]
  [1,2,3,3,4,5,6,7,8,9,10]

  >>> createInts [5,10,7]
  [5,6,7,7,8,9,10]
-}
createInts :: [Int] -> [Int]
createInts []                        = []
createInts [upper]                   = [1 .. upper]
createInts [lower, upper]            = [lower .. upper]
createInts (lower:upper:duplicate:_) = l
  where
    l                      = lowerList <> [duplicate] <> upperList
    (lowerList, upperList) = splitAt (duplicate - lower + 1) [lower .. upper]


{-|
  Returns a list of Bool with True as duplicate.

  >>> createBools
  [True,False,True]
-}
createBools :: [Bool]
createBools = [True, False, True]


{-|
  Returns a list of WeekDay with Sunday as duplicate.

  >>> createWeekDays
  [Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday]
-}
createWeekDays :: [WeekDay]
createWeekDays = Sunday : [Monday ..]


{-|
  Returns a String with all Unicode characters and 'W' as duplicate.

  >>> take 20 $ createString
  "W\NUL\SOH\STX\ETX\EOT\ENQ\ACK\a\b\t\n\v\f\r\SO\SI\DLE\DC1\DC2"
-}
createString :: String
createString = 'W' : [minBound :: Char .. maxBound :: Char]


{-|
  Outputs duplicates in lists of Bool, WeekDay, and String on 'stdout'.
-}
testDuplicates :: IO ()
testDuplicates = do
  printDuplicate' createBools
  printDuplicate' createWeekDays
  printDuplicate' createString


{-|
  'main': Evaluates the command line arguments and outputs duplicates (if any)
  in a given list. Alternatively, it executes an 'Int' list test (if '-t' is
  given) or a 'Bool' / 'weekDay' / 'Char' list test (for '-T'). For '-h', it
  outputs a short usage.
-}
main :: IO ()
main = do
  let usage = "Usage: duplicate [-t|-T] [LIST]"
  args <- getArgs
  case () of
    _
      | "-h" `elem` args -> putStrLn usage
      | "-t" `elem` args -> printDuplicate' $ createInts $ argsToInts $ drop 1 args
      | "-T" `elem` args -> testDuplicates
      | otherwise        -> printDuplicate args


-- EOF
